jQuery(function($){

// Pega as referencias
    var listaProdutos;
    var urlToAjax;
    var divRef = $("div.img-product");
    var refs = [];
    var cadaRef;
    var refsClean;
    
    function find_duplicates(arr) { 
        var len = arr.length, 
        out = [], 
        counts = {}; 
        for (var i=0;i<len;i++) { 
            var item = arr[i]; 
            var count = counts[item]; 
            counts[item] = counts[item] >= 1 ? counts[item] + 1 : 1; 
        } 
        for (var item in counts) { 
            if(counts[item] > 1) {
                out.push(item);
            }else{
                out = arr;
            }
        }
        console.log(out);
        return out;
    }

    function montaUrl() {
        
        divRef.each(function(){
            cadaRef = $(this).attr("data-referencia");
            refs.push(cadaRef);
        });

        refsClean = find_duplicates(refs);

        // Parametros to URL
        var paramRef = '/ld?refCatalogo=' + refsClean;
        // Montando URL
        var urlToAjax = "/comprafacil" + paramRef + '&idloja=59' + '&completo=1&tamanhoImagens=200';   
        return urlToAjax;
    }

    // Ajax type JSON
    function getProdutos(){
        return $.getJSON(montaUrl(refsClean));
    }

    function insereValores(){
        
        var arrProdutosParaInserir = Array.prototype.slice.call(arguments[0]);
        var numVezes;
        var valParcela;
        var refProduto;
        var nomeProduto;
        var precoPor;

        $.each(arrProdutosParaInserir, function(x, value){
            numVezes = value.parcelamento[0];
            valParcela = value.parcelamento[1];
            nomeProduto = value.nome;
            refProduto = value.referencia[0].refCatalogo;
            precoPor =  value.precoPor;
            linkCarrinho = value.linkCarrinho;

            $('div[data-referencia="'+refProduto+'"] .link-cart').attr('href', linkCarrinho);
            $('div[data-referencia="'+refProduto+'"] span.vezes').append(numVezes);
            $('div[data-referencia="'+refProduto+'"] span.fracao').append(valParcela);
            $('div[data-referencia="'+refProduto+'"] p.por span.valor').append(precoPor);
        });
    }

    function validaProdutos(){
        var produtos = Array.prototype.slice.call(arguments[0]);
            
        var listaDeProdutos = [];

        for (var i = 0; i < produtos.length; i++) {
            if (produtos[i].sucesso === "true" && produtos[i].referencia[0].saldo != 0) {
                listaDeProdutos.push(produtos[i]);
            }
        }
        insereValores(listaDeProdutos);
        return listaDeProdutos;
    }

    getProdutos().done(function(response){
        //Pega o Objeto
        listaProdutos = response.listaDeProdutos;
        validaProdutos(listaProdutos);
    }); 
});

jQuery(function($){
    $(document).scroll(function () {
        var y = $(this).scrollTop();

        if (y > 1400) {
            $(".cf_produto_comprar_flutuante").fadeIn();
        }else if (y < 1400) {
            $(".cf_produto_comprar_flutuante").fadeOut();
        };
    });
      
    $(".cf_produto_comprar_flutuante_hide").on('click', function(){
        var width = $(".cf_produto_comprar_flutuante").outerWidth();
        if($(this).hasClass("is_close")){
              $(".cf_produto_comprar_flutuante").stop().animate({right:0});
        } else {
             $(".cf_produto_comprar_flutuante").stop().animate({right:-width});   
        }

        $(this).toggleClass("is_close");
    });
});

